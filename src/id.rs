use std::fmt;
use std::cmp::Ordering;
use rand::*;

const ID_SIZE: usize = 32;
const MIN_BYTES: [u8; ID_SIZE] = [0u8; ID_SIZE];
const MAX_BYTES:[u8; ID_SIZE] = [255u8; ID_SIZE];

#[derive(Debug, Eq)]
pub struct Id {
    buf: [u8; ID_SIZE],
}

impl Id {
    pub fn new() -> Id {
        let mut buf = [0u8; ID_SIZE];
        rand::thread_rng().fill_bytes(&mut buf);

        Id{
            buf
        }
    }

    pub fn from_bytes(bytes: &[u8]) -> Option<Id> {
        if bytes.len() != ID_SIZE {
            return None;
        }
        let mut buf = [0u8; ID_SIZE];
        &buf[..].copy_from_slice(bytes);

        Some(Id{
            buf,
        })
    }

    pub fn min() -> Id {
        MIN_BYTES.into()
    }

    pub fn max() -> Id {
        MAX_BYTES.into()
    }

    pub fn distance_from(&self, other: &Id) -> [u8; ID_SIZE] {
        let mut d = [0u8; ID_SIZE];
        for i in 0..ID_SIZE {
            d[i] = self.buf[i] ^ other.buf[i];
        }

        d
    }

    /// Check whether the Id and the bytes contained in `buf` share a prefix of
    /// at least `bit_len` bits.
    ///
    /// Since the Ids are interpreted as 256-bit Little-Endian integers, a "prefix"
    /// begins at the final byte of each value, so we compare the bits of each byte in
    /// the current Id and the comparison bytes in reverse order.
    ///
    /// Panics if bit_len is larger than the the number of bits that can be contained
    /// in either buf or the Id.
    pub fn has_prefix(&self, buf: &[u8], bit_len: usize) -> bool {
        // Iterate from last bit to first
        for i in (1..(bit_len+1)).rev() {
            // Offset from end of buffer for bytes to test
            let byte_offset = ((bit_len - i) / 8 + 1) as usize;
            let bit_i = ((bit_len - i) % 8) as usize;

            let self_byte = self.buf[self.buf.len() - byte_offset];
            let self_bit = (self_byte & (1 << (7-bit_i))) >> (7-bit_i);

            let other_byte = buf[buf.len() - byte_offset];
            let other_bit = (other_byte & (1 << (7-bit_i))) >> (7-bit_i);

            if self_bit != other_bit {
                return false
            }
        }

        true
    }
}

impl<'a> IntoIterator for &'a Id {
    type Item = u8;
    type IntoIter = IdIntoIterator<'a>;

    fn into_iter(self) -> IdIntoIterator<'a> {
        IdIntoIterator {
            id: self,
            cur: 0,
            cur_byte: 0,
        }
    }
}

#[derive(Debug)]
pub struct IdIntoIterator<'a> {
    id: &'a Id,
    cur: usize,
    cur_byte: u8,
}

impl<'a> Iterator for IdIntoIterator<'a> {
    type Item = u8;

    /// Get each successive bit of the ID as a u8.
    /// Since we interpret the id as a Little Endian integer but
    /// iterate starting with the most significant bit, we get the
    /// bits from most significant to least significant in each byte
    /// from the highest indexed to the lowest indexed.
    /// (we would probably need less math if we interpreted the bytes
    /// as a big-endian integer)
    fn next(&mut self) -> Option<u8> {
        let bit_i = self.cur % 8;
        if bit_i == 0 {
            if self.cur >= ID_SIZE {
                return None;
            }
            let idx = ID_SIZE - 1 - self.cur/8;
            self.cur_byte = self.id.buf[idx];
        }
        let shift = 7-bit_i;
        self.cur += 1;

        Some((self.cur_byte & (1 << shift)) >> shift)
    }
}

impl PartialEq for Id {
    fn eq(&self, other: &Id) -> bool {
        self.buf.iter().zip(other.buf.iter()).all(|(a,b)| a == b)
    }
}

impl PartialOrd for Id {
    fn partial_cmp(&self, other: &Id) -> Option<Ordering> {
        for i in (0..ID_SIZE).rev() {
            let self_byte = self.buf[i];
            let other_byte = other.buf[i];

            if self_byte < other_byte {
                return Some(Ordering::Less);
            }
            if self_byte > other_byte {
                return Some(Ordering::Greater);
            }
        }

        Some(Ordering::Equal)
    }
}

impl From<[u8; ID_SIZE]> for Id {
    fn from(data: [u8; ID_SIZE]) -> Self {
        Id{
            buf: data,
        }
    }
}

impl fmt::Display for Id {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let bytes_as_hex: Vec<String> = self.buf.iter()
                               .map(|b| format!("{:02X}", b))
                               .collect();
        write!(f, "#<{}>", bytes_as_hex.join(" "))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn initialize_random_id() {
        let n1 = Id::new();
        let n2 = Id::new();

        assert_ne!(n1, n2);
    }

    #[test]
    fn should_initialize_from_246_bit_bytearray() {
        let bs = [0u8; 32];
        let _n: Id = bs.into();
    }

    #[test]
    fn should_initialize_from_246_bit_byte_vector() {
        let buf: Vec<u8> = vec!(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        Id::from_bytes(&buf).unwrap();
    }

    #[test]
    fn should_reject_buffer_that_is_not_256_bits() {
        let buf: Vec<u8> = vec!(1,2,3);
        assert_eq!(Id::from_bytes(&buf), None);
    }

    #[test]
    fn id_equality() {
        let n: Id = [0u8; 32].into();
        let n_eq: Id = [0u8; 32].into();
        let n_ne: Id = [1u8; 32].into();

        assert_eq!(n, n_eq);
        assert_ne!(n, n_ne);
    }

    #[test]
    fn min_value() {
        assert_eq!(Id::min(), [0u8; 32].into());
    }

    #[test]
    fn max_value() {
        assert_eq!(Id::max(), [255u8; 32].into());
    }

    #[test]
    fn comparability() {
        assert!(Id::min() < Id::max());
        assert!(Id::max() > Id::min());
    }

    #[test]
    fn represents_ids_with_little_endian_byte_ordering() {
        let mut bs1 = [0u8; 32];
        let mut bs2 = [0u8; 32];
        bs1[0] = 255;
        bs2[1] = 1;

        let n1: Id = bs1.into();
        let n2: Id = bs2.into();

        assert!(n1 < n2);
    }

    #[test]
    fn represents_ids_with_native_bit_ordering() {
        let mut bs1 = [0u8; 32];
        let mut bs2 = [0u8; 32];
        bs1[0] = 10;
        bs2[0] = 11;

        let n1: Id = bs1.into();
        let n2: Id = bs2.into();

        assert!(n1 < n2);
    }

    #[test]
    fn gets_distance_as_xor_metric() {
        let buf1 = vec!(255u8,255,255,255,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        let buf2 = vec!(0u8,0,255,255,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        let xor = &[255u8,255,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
        let n1 = Id::from_bytes(&buf1).unwrap();
        let n2 = Id::from_bytes(&buf2).unwrap();

        let distance_equals_xor = n1.distance_from(&n2).iter()
            .zip(xor.iter())
            .all(|(a,b)| a == b);

        assert!(distance_equals_xor);
    }

    #[test]
    fn testing_bit_prefix() {
        // Prefix: 00001111 00001111...
        let n: Id = [255,255,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,15,15].into();

        // Test prefix: 0
        assert!(n.has_prefix(&[0u8], 1));

        // Test prefix: 00000
        assert!(!n.has_prefix(&[0u8], 5));

        // Test prefix: 00001111
        assert!(n.has_prefix(&[15u8], 8));

        // // Test prefix: 00001111 0000
        assert!(n.has_prefix(&[0u8, 15], 12));

        // Test prefix: 00001111 00000000
        assert!(!n.has_prefix(&[0u8, 15], 16));
    }

    #[test]
    fn iterates_bits() {
        // byte 1: 147 = 10010011
        // byte 2: 212 = 11010100
        let n: Id = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,212,147].into();

        let mut bits = Vec::new();
        for bit in &n {
            bits.push(bit);
        }

        assert_eq!(vec!(1u8, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0), &bits[0..16]);
        assert_eq!(0u8, bits[16..].into_iter().sum());
    }
}
