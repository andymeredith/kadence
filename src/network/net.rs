use super::node::Node;

pub trait Network {

    fn join(&mut self, node: Node);
}