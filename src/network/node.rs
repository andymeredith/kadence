use std::net::SocketAddr;
use crate::id::Id;
use super::net::Network;
use std::io;

#[derive(Debug)]
pub(crate) struct Node<'a, T: Network + 'a> {
    id: Id,
    network: &'a T,
    address: SocketAddr,
}

impl<'a, T: Network + 'a> Node<'a, T> {

    fn new(id: Id, network: &'a T, address: SocketAddr) -> Node<'a, T> {
        Node {
            address,
            network,
        }
    }

    fn connect(&mut self, port: u32) -> io::Result<()> {
        Ok(())
    }
}
